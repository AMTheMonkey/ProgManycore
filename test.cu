#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <string.h>
#include <errno.h>
#include <fcntl.h> // for open



#define OUTFILE "mandelbrot_parallel.out"

#define XMIN -2.0
#define YMIN -2.0
#define XMAX 2.0
#define YMAX 2.0
#define RESOLUTION 0.01
#define NITERMAX 400
#define DEVICE_MEM_RATIO (7ull)/(8ull)

//ccc_mprun -n 1 -p dgx -s
//module load cuda

/* Calcul de tous les points de notre grille */
__global__ void calcul_points(int* itertab, int N) {
  int xpixel = blockIdx.x;
  int ypixel = threadIdx.x;


  if(ypixel * xpixel < N) {
   double xinit = XMIN + xpixel * RESOLUTION;
   double yinit = YMIN + ypixel * RESOLUTION;
   double x = xinit;
   double y = yinit;
   int iter = 0;
   for (iter = 0; iter < NITERMAX; iter++) {
      double prevy = y, prevx = x;

     if ( (x * x + y * y) > 4.0)
        break;
      x = prevx * prevx - prevy * prevy + xinit;
      y = 2.0 * prevx * prevy + yinit;
    }
    itertab[xpixel * blockDim.x + ypixel] = iter;
    
  }
}

/* Calcul d'une partie de notre grille */
__global__ void calcul_points_partiel(int* itertab, int index_blocks, int index_threads, int nbpixelx, int nbpixely) {
  int xpixel = blockIdx.x + index_blocks;
  int ypixel = threadIdx.x + index_threads;

  if(xpixel < nbpixelx && ypixel < nbpixely) {
    double xinit = XMIN + xpixel * RESOLUTION;
    double yinit = YMIN + ypixel * RESOLUTION;
    double x = xinit;
    double y = yinit;
    int iter = 0;
    for (iter = 0; iter < NITERMAX; iter++) {
        double prevy = y, prevx = x;

      if ( (x * x + y * y) > 4.0)
        break;
      x = prevx * prevx - prevy * prevy + xinit;
      y = 2.0 * prevx * prevy + yinit;
    }
    itertab[blockIdx.x * blockDim.x + threadIdx.x] = iter;  
  }
}

int main(int argc, char **argv)
{
  int * itertab;
  int nbpixelx;
  int nbpixely;
  int xpixel = 0, ypixel = 0;
  FILE * file;

  /* Variables pour la copies de données sur le device */
  int* d_itertab;
  dim3 blocks, threads;



  int size_int = sizeof(int);

  /*calcul du nombre de pixel*/
  nbpixelx = ceil((XMAX - XMIN) / RESOLUTION);
  nbpixely = ceil((YMAX - YMIN) / RESOLUTION);
  /*allocation du tableau de pixel*/
  blocks.x = nbpixelx;
  blocks.y = 1;
  blocks.z = 1;

  threads.x = nbpixely;
  threads.y = 1;
  threads.z = 1;


    int device;
    cudaDeviceProp gpu_prop;

    /* cuda sizing */
    cudaGetDevice(&device);
    if (cudaGetDeviceProperties(&gpu_prop, device) != cudaSuccess) {
        printf("Error on getting gpu properties !\n");
        exit(EXIT_FAILURE);
    }
  
  if ( (itertab = (int*) malloc(size_int * nbpixelx * nbpixely)) == NULL)
  {
    printf("ERREUR d'allocation de itertab, errno : %d (%s) .\n", errno, strerror(errno));
    return EXIT_FAILURE;
  }

  /* Allocution de la mémoire sur le device */
  int tranchex = 300;
  int tranchey = 300;

    if (cudaMalloc((void**)&d_itertab, size_int * tranchex * tranchey)
              != cudaSuccess) {
          printf("Error on allocating max_index_gpu 2!\n");
          exit(EXIT_FAILURE);
      }



  for (int index_blocks = 0; index_blocks < nbpixelx; index_blocks+=tranchex)
  {
    if(index_blocks + tranchex > nbpixelx) tranchex = nbpixelx - index_blocks; //Si notre tranche est trop grande pour le nombre de pixel x restants

    for (int index_threads = 0; index_threads < nbpixely; index_threads+=tranchey)
    {
      if(index_threads + tranchey > nbpixely) tranchey = nbpixely - index_threads; // Si notre tranche y est trop grande pour le nombre de pixel y restants
      cudaMemset(d_itertab,-1, size_int * tranchex * tranchey);

      calcul_points_partiel<<<tranchex, tranchey>>>(d_itertab, index_blocks, index_threads,nbpixelx,nbpixely);
      cudaDeviceSynchronize();

      //Copie des morceaux de colonnes calculées dans le tableau itertab. Nous jouons avec les pointeurs pour régler les problèmes d'alignement
      for (int i = 0; i < tranchex; ++i)
      {
       if (cudaMemcpy(itertab + ((index_blocks + i) * nbpixely + index_threads), d_itertab + i * tranchey, size_int * tranchey, cudaMemcpyDeviceToHost) != cudaSuccess) {
          printf("Error on copying itertab to host !\n");
          printf("%s\n",cudaGetErrorString(cudaGetLastError()));
          exit(EXIT_FAILURE);
        }
      }
      tranchey = 300; //On remet notre tranche de pixel y à la valeur initiale pour les autres pixels x

    }
  }

  /*output des resultats compatible gnuplot*/
    
  if ( (file = fopen(OUTFILE, "w")) == NULL ) {
    printf("Erreur à l'ouverture du fichier de sortie : errno %d (%s) .\n", errno, strerror(errno));
    return EXIT_FAILURE;
  }
  for (xpixel = 0; xpixel < nbpixelx; xpixel++)
  {
    for (ypixel = 0; ypixel < nbpixely; ypixel++) {
      double x = XMIN + xpixel * RESOLUTION;
      double y = YMIN + ypixel * RESOLUTION;
      fprintf(file, "%f %f %d\n", x, y, itertab[xpixel * nbpixely + ypixel]);
    }
    fprintf(file, "\n");
  }
  fclose(file);
  /*sortie du programme*/

  free(itertab);
  cudaFree(d_itertab);


  return EXIT_SUCCESS;
}